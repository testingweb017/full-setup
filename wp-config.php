<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

if ( file_exists( ABSPATH . 'wp-config-local.php' ) ):
	require_once( ABSPATH . 'wp-config-local.php' );
else:

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'carthage' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '|vfTSwi}qj(UM-=EDeqD6i/NgCbCSwf*a6(8zs;..9HKV2Or:Y-D&U3/G!A#zd,n' );
define( 'SECURE_AUTH_KEY',  'x nsF$~JsWx7fVEr1.k/OENoa)OI(F{#`ahozbJ3&n]YA_`ayd2Yu-ciXC1Rmd>6' );
define( 'LOGGED_IN_KEY',    ';~]m$T]P)o_U@=^`Ict8RmD<}&F((F)lzWIt(#1 ]J9Oi>58q*SrKRL]W9HA{=y^' );
define( 'NONCE_KEY',        'QCl*R&/)>.iEfjRkF:6Cuf&*{lxy)?tUdS+7fRJ; `/#R1+^gO]<1o.!EZeNKJDj' );
define( 'AUTH_SALT',        'p^Uk jG>rl$Lg>]O<b[5(2*2G%9otFx)g` gMu`w^]C8n(G@nigqd,|-SiSM7uTY' );
define( 'SECURE_AUTH_SALT', 'KFlI]ZkT<{@$/99.FAgx@aT&oiJi,0&<&m${Au7O~dP1(::YDB84g0^z6O!O&Uve' );
define( 'LOGGED_IN_SALT',   'Eci/QOp6Tob6E4J7<OuwAeH@J_IC6VYP*<o?_VA80*+l4:ssX$#*s&k3W:`T6>is' );
define( 'NONCE_SALT',       '5H;h<pG7p~>uTo`LP/#3ki G,b6oCj1k[=cKUw)O3<E?wZ42{04IrKSm1j7OUgBS' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

endif;