jQuery( document ).ready( function( $ ) {


  var deviceScreenWidth = screen.width;

  if(deviceScreenWidth>991){

    //home page animation

    let controller = new ScrollMagic.Controller();
    let timeline = new TimelineMax();

    timeline
    .from('#hWave', 6, {
      x: -220
    })
    .to('#hContent', 6, {
      x: 280
    }, '-=6')
    .to('#more', 6, {
      x: 250
    }, '-=6')
    
    
    .from('#overline', 2, {
      top: '100%'
    }, '-=2')
    .from('#hBtn',3, {
      top: '20px',
      autoAlpha: 0
    }, '-=5')
    .to('#heroContentCont1', 10, {
      y: '-150%'
    }, '7')
    .from('#heroContentCont2', 10,{
      autoAlpha: 0,
      top: '100%'
    },'+8')
    .to('#heroRightNotice', 10,{
      'left': 0,
      autoAlpha: 1
    },'+8')


    let scene = new ScrollMagic.Scene({
      triggerElement: '.homePageHero',
      duration: '150%',
      triggerHook: 0,
    })
    .setTween(timeline)
    .setPin('.homePageHero')
    .setClassToggle("#homeHeroCont", "hideBg")
    .addTo(controller)

  }


/*
let controller2 = new ScrollMagic.Controller();

$('.fadeInUp').each(function(){

    let fadeInScene = new ScrollMagic.Scene({
      triggerElement: this,
      offset: 50,
      triggerHook: 0.9,
      reverse: false
    })
    .setClassToggle(this, 'active')
    .addTo(controller2);



});*/








  $('.accordbtn').click(function(){

    var target = $(this).data('accord-target');
    var panel = $('#'+target);


    var hideAll = target.split("-");
    $('[id^='+hideAll[0]+']').removeClass('active');
    //$('.accordbtn').removeClass('active');
    $(this).parent().find('.accordbtn').removeClass('active');
    $(this).toggleClass('active');

    panel.addClass('active');
    panel.css('max-height',panel.prop('scrollHeight')+'px');



  });



  $('.btn').click(function(e){
    e.preventDefault();
    var getModalId = $(this).data('modal');

    $('#'+getModalId).addClass('open');
  });


  $('.closeBtn').click(function(e){
    e.preventDefault();
    var getModalId = $(this).data('target-modal');
    $('#'+getModalId).removeClass('open');
  });


});


new WOW().init();