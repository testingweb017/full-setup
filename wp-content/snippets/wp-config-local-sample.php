<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
define( 'DB_NAME', '' );
define( 'DB_USER', '' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', '' );
define( 'WP_SITEURL', '' );
define( 'WP_HOME', '' );
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gr)3U?{Y6WSJ5R87Tvw5FV#z!xThjSN+t;n9A`O_H+4m76,s.jNGHI} `oS+gpEM');
define('SECURE_AUTH_KEY',  '2f}!|8Gyg6p_Z#io.!~(-,)Kj%Df}&`P9N>DPd!!DWN)nXJQ?!]iT?2I _Y V9we');
define('LOGGED_IN_KEY',    '?G%QnR8tyceFl2[N<4!(^tCI 3{}p*M]G&Y 2>t%:o<EU8uo?+Kw(!GxOOs<FQ`5');
define('NONCE_KEY',        '%~QoO>E@ws9o1L:{g:UOn=OI6,.[XeTGXE?$}rgsi=<k;N58<nn]Gx5@M`4=+j7X');
define('AUTH_SALT',        ' %7:GV|kPsE9A#8V<N V[4mt (ZT|^$LK5o@vsLyF9_5NX kq_>8PA?O/9FBr+Uc');
define('SECURE_AUTH_SALT', 'exJ+*E7!DLUPqeCbe<U3(/nB5mLw`4TF*oawN20@A*o@vyCZBKN~^TC3,%:#Q8H2');
define('LOGGED_IN_SALT',   'J*lBxI#Qm0fLxnW2.w/iG{A&P!bhZAFcQ[O}!7yXEva!!Qodeed+M*v|Ew!5GXAw');
define('NONCE_SALT',       '9ODLsiJIOv: KX.~yyE8?i[)H]D%(O!YlO0`~OqmDrw9LF2TSx2Z[x*|Cwd/!`^Q');

$table_prefix  = 'wp_';

define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
